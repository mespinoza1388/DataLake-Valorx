from datamesh.services.base_service import base_service
from datamesh.artifactory.entities.raw import raw 
from datamesh.artifactory.entities.stage import stage
class light_transformation_base_service(base_service):
    def __init__(self, environment, schemas, config = None): 
        super().__init__(environment, schemas, config)    
        self.__data_config = self.get_config()  
        obj_raw = raw(self.__data_config['datalake'])
        obj_stage = stage(self.__data_config['datalake'])
        self.__data_entity = {}
        self.__data_entity['raw_tables'] = obj_raw.tables()
        self.__data_entity['raw_columns'] = obj_raw.columns()
        self.__data_entity['stage_tables'] = obj_stage.tables()
        self.__data_entity['stage_columns'] = obj_stage.columns()
     
    def get_raw_tables(self, table_type=None, table_name=None, schema_group=None, table_priority=None, process_id=None, status='a'):
        data = []
        raw_tables = self.__data_entity['raw_tables']
        for raw_table in raw_tables:
            if (raw_table['table_type'].upper() == table_type.upper() if table_type is not None else raw_table['table_type'].upper()) \
                and (raw_table['table_name'].upper() == table_name.upper() if table_name is not None else raw_table['table_name'].upper()) \
                and (raw_table['schema_group'].upper() == schema_group.upper() if schema_group is not None else raw_table['schema_group'].upper()) \
                and (str(raw_table['table_priority']) == str(table_priority) if table_priority is not None else str(raw_table['table_priority'])) \
                and (str(raw_table['process_id']) == str(process_id)if process_id is not None else str(raw_table['process_id'])) \
                and (raw_table['status'].upper() == status.upper() if status is not None else raw_table['status'].upper()):
                data.append(raw_table)
        return data 
    
    def get_raw_columns(self, table_name=None, schema_group=None, status='a'):
        data = []
        raw_columns = self.__data_entity['raw_columns']
        for raw_column in raw_columns:
            if (raw_column['table_name'].upper() == table_name.upper() if table_name is not None else raw_column['table_name'].upper()) \
                and (raw_column['schema_group'].upper() == schema_group.upper() if schema_group is not None else raw_column['schema_group'].upper()) \
                and (raw_column['status'].upper() == status.upper() if status is not None else raw_column['status'].upper()):
                data.append(raw_column)
        return data 
    
    def get_stage_tables(self, table_type=None, table_name=None, schema_group=None, table_priority=None, process_id=None, status='a'): 
        data = []
        stage_tables = self.__data_entity['stage_tables'] 
        for stage_table in stage_tables:
            if (stage_table['table_type'].upper() == table_type.upper() if table_type is not None else stage_table['table_type'].upper()) \
                and (stage_table['table_name'].upper() == table_name.upper() if table_name is not None else stage_table['table_name'].upper()) \
                and (stage_table['schema_group'].upper() == schema_group.upper() if schema_group is not None else stage_table['schema_group'].upper()) \
                and (str(stage_table['table_priority']) == str(table_priority) if table_priority is not None else str(stage_table['table_priority'])) \
                and (str(stage_table['process_id']) == str(process_id)if process_id is not None else str(stage_table['process_id'])) \
                and (stage_table['status'].upper() == status.upper() if status is not None else stage_table['status'].upper()):
                data.append(stage_table)
        return data 
    
    def get_stage_columns(self, table_name=None, schema_group=None, status='a'):
        data = []
        stage_columns = self.__data_entity['stage_columns']
        for stage_column in stage_columns:
            if (stage_column['table_name'].upper() == table_name.upper() if table_name is not None else stage_column['table_name'].upper()) \
                and (stage_column['schema_group'].upper() == schema_group.upper() if schema_group is not None else stage_column['schema_group'].upper()) \
                and (stage_column['status'].upper() == status.upper() if status is not None else stage_column['status'].upper()):
                data.append(stage_column)
        return data 
  
    def get_script_drop_table_stage(self, stage_table):
        scripts = []
        scripts.append('DROP TABLE IF EXISTS ' + self.get_schema_stage_athena() + '.' + stage_table['table_name'] ) 
        return scripts

    def get_script_repair_table_stage_transactional(self, stage_table):        
        scripts = []
        scripts.append('MSCK REPAIR TABLE ' + self.get_schema_stage_athena() + '.' + stage_table['table_name'] ) 
        return scripts
          
    def get_schema_raw_athena(self):
        return self.__data_config['schema']['team'] + '_' \
            + self.__data_config['schema']['datasource'] + '_' + self.__data_config['schema']['schema'] + '_raw'
    
    def get_path_full_raw_s3(self):
        return 's3://' + self.__data_config['datalake']['bucket_raw'] + '/' \
            + self.__data_config['schema']['team'] + '/' + self.__data_config['schema']['datasource'] + '/' + self.__data_config['schema']['schema'] 
    
    def get_path_raw_s3(self):
        return self.__data_config['schema']['team'] + '/' + self.__data_config['schema']['datasource'] + '/' + self.__data_config['schema']['schema']
          
    def get_schema_stage_athena(self):
        return self.__data_config['schema']['team'] + '_' \
            + self.__data_config['schema']['datasource'] + '_' + self.__data_config['schema']['schema'] + '_stage'
    
    def get_path_full_stage_s3(self):
        return 's3://' + self.__data_config['datalake']['bucket_stage'] + '/' \
            + self.__data_config['schema']['team'] + '/' + self.__data_config['schema']['datasource'] + '/' + self.__data_config['schema']['schema'] 
    
    def get_path_stage_s3(self):
        return self.__data_config['schema']['team'] + '/' + self.__data_config['schema']['datasource'] + '/' + self.__data_config['schema']['schema']
     
    def get_script_create_table_stage_master(self, stage_table, stage_columns):
        scripts = []  
        stage_columns_nonpartition = []   
        for stage_column in stage_columns:
            stage_columns_nonpartition.append(stage_column['column_name'] + ' ' + stage_column['column_type'])
        script_nonpartition = "(" + " ,".join(stage_columns_nonpartition) + ")"
        scripts.append('CREATE EXTERNAL TABLE IF NOT EXISTS ' + self.get_schema_stage_athena() + '.' + stage_table['table_name'])         
        scripts.append(script_nonpartition)  
        scripts.append('ROW FORMAT SERDE "org.apache.hadoop.hive.ql.io.parquet.serde.ParquetHiveSerDe"')
        scripts.append('STORED AS INPUTFORMAT "org.apache.hadoop.hive.ql.io.SymlinkTextInputFormat"')
        scripts.append('OUTPUTFORMAT "org.apache.hadoop.hive.ql.io.HiveIgnoreKeyTextOutputFormat"')
        scripts.append('LOCATION "' + self.get_path_full_stage_s3() + '/' + stage_table['table_name'] + \
            '/_symlink_format_manifest/";')
        return scripts
    
    def get_script_create_table_stage_transactional(self, stage_table, stage_columns):
        scripts = [] 
        stage_columns_partition = []
        stage_columns_nonpartition = []
        for stage_column in stage_columns:
            if stage_column['is_partition'] == 't':
                stage_columns_partition.append(stage_column['column_name'] + ' ' + stage_column['column_type'])
            else:
                stage_columns_nonpartition.append(stage_column['column_name'] + ' ' + stage_column['column_type'])
        
        script_nonpartition = "(" + " ,".join(stage_columns_nonpartition) + ")"   
        script_partition = "" 
        if len(stage_columns_partition) > 0:
            script_partition = "PARTITIONED BY (" + " ,".join(stage_columns_partition) + ")"   
             
        scripts.append('CREATE EXTERNAL TABLE IF NOT EXISTS ' + self.get_schema_stage_athena() + '.' + stage_table['table_name'])         
        scripts.append(script_nonpartition) 
        scripts.append(script_partition)
        scripts.append('ROW FORMAT SERDE "org.apache.hadoop.hive.ql.io.parquet.serde.ParquetHiveSerDe"')
        scripts.append('STORED AS INPUTFORMAT "org.apache.hadoop.hive.ql.io.SymlinkTextInputFormat"')
        scripts.append('OUTPUTFORMAT "org.apache.hadoop.hive.ql.io.HiveIgnoreKeyTextOutputFormat"')
        scripts.append('LOCATION "' + self.get_path_full_stage_s3() + '/' + stage_table['table_name'] + \
            '/_symlink_format_manifest/";')
        return scripts
    
    
        
