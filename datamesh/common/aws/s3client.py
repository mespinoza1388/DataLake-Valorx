from datamesh.common.aws.base import base
class s3client(base):
    def __init__(self, config = None):
         super().__init__(config)
         self.__client_s3 = self.session.client('s3')

    def get_object(self, bucket_name, key_path):
        try:             
            response = self.__client_s3.get_object(Bucket=bucket_name, Key=key_path)
            status_code = response.get("ResponseMetadata", {}).get("HTTPStatusCode")
            if status_code == 200:
                return True, response.get("Body")
            else:
                return False, response.get("Body")
        except Exception as e:
            return False, str(e)

    def delete_folder_and_objects_inside(self, bucket_name, key_path):
        try: 
            objects_to_delete = self.__client_s3.list_objects(Bucket=bucket_name, Prefix=key_path)        
            delete_keys = {'Objects' : []}
            delete_keys['Objects'] = [{'Key' : k} for k in [obj['Key'] for obj in objects_to_delete.get('Contents', [])]] 
            response = self.__client_s3.delete_objects(Bucket=bucket_name, Delete=delete_keys)
            return True, response
        except Exception as e:
            return False, str(e)    

    def upload_file(self, bucket_name, key_path, local_path):
        try: 
            response = self.__client_s3.upload_file(local_path, bucket_name, key_path)
            return True, response
        except Exception as e:
            return False, str(e)    
        
    def get_list_objects(self, bucket_name, key_path):
        list = []  
        paginator = self.__client_s3.get_paginator('list_objects_v2') 
        page_iterator = paginator.paginate(Bucket=bucket_name)
        for page in page_iterator:
            if 'Contents' in page:
                for content in page['Contents']:   
                    if key_path in content['Key']:
                        list.append(content['Key'])
        return list   