import datamesh.artifactory.config.constants as constants
class stage():     
    def __init__(self, config_datalake = None):  
        index_col_tables = constants.entities_stage_index_col_tables
        index_col_columns = constants.entities_stage_index_col_columns
        file_tables = constants.entities_stage_tables
        file_columns = constants.entities_stage_columns
        separator = constants.entities_separador
        header = constants.entities_header 
        if not config_datalake is None:  
            if constants.entities_type_source == 'aws_s3':                
                bucket_name = config_datalake['bucket_artifactory']
                path_key = config_datalake['path_artifactory_stage']  
                status, data = self.__get_data_s3(file_tables, separator, index_col_tables, header, bucket_name, path_key) 
                if status:
                    self.__data_tables = data
                else:
                    raise Exception(data)
                status, data = self.__get_data_s3(file_columns, separator, index_col_columns, header, bucket_name, path_key) 
                
                if status:
                    self.__data_columns = data
                else:
                    raise Exception(data)
        else:
            raise NotImplementedError('Not implement function')

    def __get_data_s3(self, file_name, separator, index_col, header, bucket_name, path_key): 
        from datamesh.common.aws.s3client import s3client
        import pandas as pd
        s3_client = s3client()
        status_code, file_tables = s3_client.get_object(bucket_name, path_key + file_name)  
        if status_code:
            try:
                df = pd.read_csv(file_tables, sep=separator, index_col = index_col, header = header)
                df = df.fillna('')
                df = df.sort_index()
                return True, df
            except Exception as ex:
                return False, str(ex)
        else:
            return False, file_tables
          
    def tables(self, table_type=None, table_name=None, schema_group=None, status='a'): 
        data = [] 
        if not self.__data_tables is None:
            df = self.__data_tables
            for i in df.index: 
                if (str(df['table_type'][i]).upper() == table_type.upper() if table_type is not None else str(df['table_type'][i]).upper()) \
                    and (str(i[1]).upper() == table_name.upper() if table_name is not None else str(i[1]).upper()) \
                    and (str(i[0]).upper() == schema_group.upper() if schema_group is not None else str(i[0]).upper()) \
                    and (str(df['status'][i]).upper() == status.upper() if status is not None else str(df['status'][i]).upper()):
                    data.append({
                        'schema_group' : i[0],
                        'table_name' : i[1], 
                        'table_type' : df['table_type'][i],
                        'table_priority' : df['table_priority'][i],
                        'process_id' : df['process_id'][i],
                        'source_table_name' : df['source_table_name'][i], 
                        'status' : df['status'][i]
                    })
        else:
            raise Exception('error cargando stage_tables')
        return data 
 
    def columns(self, table_name=None, schema_group=None, status='a'): 
        data = []   
        if not self.__data_columns is None:
            df = self.__data_columns
            for i in df.index:  
                if (str(i[1]).upper() == table_name.upper() if table_name is not None else str(i[1]).upper()) \
                    and (str(i[0]).upper() == schema_group.upper() if schema_group is not None else str(i[0]).upper()) \
                    and (str(df['status'][i]).upper() == status.upper() if status is not None else str(df['status'][i]).upper()):
                    data.append({
                        'schema_group' : i[0],
                        'table_name' : i[1],
                        'column_id' : i[2],
                        'column_name' : df['column_name'][i],
                        'column_type' : df['column_type'][i], 
                        'source_table_name' : df['source_table_name'][i],
                        'source_column_name' : df['source_column_name'][i],
                        'source_column_type' : df['source_column_type'][i],
                        'is_pk_table' : df['is_pk_table'][i],
                        'is_partition_table' : df['is_partition_table'][i],
                        'is_incremental_table' : df['is_incremental_table'][i],
                        'is_partition_by_unique' : df['is_partition_by_unique'][i],
                        'is_order_by_unique' : df['is_order_by_unique'][i],
                        #'is_partition_by' : df['is_partition_by'][i],
                        #'is_order' : df['is_order'][i],
                        #'rank_order' : df['rank_order'][i],
                        #'type_order' : df['type_order'][i],
                        #'is_column_incremental' : df['is_column_incremental'][i],
                        'fn_transform1' : df['fn_transform1'][i],
                        'fn_transform_name1' : df['fn_transform_name1'][i],
                        'fn_transform_output_column1' : df['fn_transform_output_column1'][i], 
                        'fn_transform_input_column1' : df['fn_transform_input_column1'][i],
                        'fn_transform_input_parameter1' : df['fn_transform_input_parameter1'][i], 
                        'fn_transform_input_default1' : df['fn_transform_input_default1'][i],
                        'fn_transform2' : df['fn_transform2'][i],
                        'fn_transform_name2' : df['fn_transform_name2'][i],
                        'fn_transform_output_column2' : df['fn_transform_output_column2'][i], 
                        'fn_transform_input_column2' : df['fn_transform_input_column2'][i],
                        'fn_transform_input_parameter2' : df['fn_transform_input_parameter2'][i], 
                        'fn_transform_input_default2' : df['fn_transform_input_default2'][i],
                        'fn_transform3' : df['fn_transform3'][i],
                        'fn_transform_name3' : df['fn_transform_name3'][i],
                        'fn_transform_output_column3' : df['fn_transform_output_column3'][i], 
                        'fn_transform_input_column3' : df['fn_transform_input_column3'][i],
                        'fn_transform_input_parameter3' : df['fn_transform_input_parameter3'][i], 
                        'fn_transform_input_default3' : df['fn_transform_input_default3'][i],
                        'fn_function_name' : df['fn_function_name'][i], 
                        'fn_function_column' : df['fn_function_column'][i],
                        'status' : df['status'][i] 
                    }) 
        else:
            raise Exception('error cargando stage_columns')
        return data  
 