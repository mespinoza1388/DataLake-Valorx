def list_find(column, value, list): 
    row = None 
    for ele in list:
        if ele[column] == value:
            row = ele
    return row

def list_findall(column, value, list): 
    list_find = []
    for ele in list:
        if ele[column] == value:
            list_find.append(ele)
    return list_find

def list_addcolumn(column, value, list):
    for ele in list:
        ele[column] = value        
    return list

def list_leftjoin(listA, listB, index):
    list = []
    for eleA in listA: 
        sublistB = list_findall(index, eleA[index], listB)   
        for colA in eleA:
            if colA != index: 
                sublistB = list_addcolumn(colA, eleA[colA], sublistB) 
        list = list + sublistB
    return list

def list_findall_by_index(column, value, list, index):
    data = []
    i = 0
    for ele in list:
        if i >= index:
            if (ele[column].upper() == value.upper() if value is not None else ele[column].upper()):
                data.append(ele)
        i = i + 1
    return data