from datetime import date, datetime, timedelta 
import re 
from pyspark.sql.functions import udf 
from pyspark.sql.types import * 

def fn_transform_Replace(value, parameter): 
    if value is None:
        return None
    else:
        if '->' in parameter:
            return str(value).replace(parameter.split('->')[0], parameter.split('->')[1])
        else:
            return str(value) 

def fn_transform_ByteMagic(value, value_default): 
    current_value = value_default
    if value == "b'T'":
        current_value = 'T'
    elif value == "b'F'":
        current_value = 'F' 
    return current_value

def fn_transform_ClearDouble(value, value_default): 
    try:
        if not (value is None or str(value) == "None"):
            current_value = float(value)
        else:
            if value_default is None:
                current_value = None
            else:
                current_value = float(value_default)
    except:
        if value_default is None:
            current_value = None
        else:
            current_value = float(value_default)
    return current_value

def fn_transform_ClearString(value, value_default):    
    try:
        if not (value is None or str(value) == "None"):
            current_value = str(value).strip()
        else:
            if value_default is None:
                current_value = None
            else:
                current_value = str(value_default).strip()
    except:
        if value_default is None:
            current_value = None
        else:
            current_value = str(value_default).strip()
    return current_value

def fn_convert_datetime_magic_to_datetime(date, time = None):
    if time is None:
        time = '000000'
    int_date = int(date)     
    return datetime(1900, 1, 1, int(time[0:2]), int(time[2:4]), int(time[4:6])) + timedelta(days = int_date - 693596) 

def fn_transform_DateMagic(date_magic, format, value_default):
    date_pattern = r'^([7-9]\d{5}|[1-2]\d{6}|3[0-5]\d{5})$'
    try:
        if bool(re.match(date_pattern, str(date_magic))):
            return fn_convert_datetime_magic_to_datetime(date_magic)
        else: 
            return datetime.strptime(value_default, format)
    except:
        return None

def fn_transform_DatetimeMagic(date_magic, time_magic, format, value_default):
    date_pattern = r'^([7-9]\d{5}|[1-2]\d{6}|3[0-5]\d{5})$'
    time_pattern = r'^([01][0-9]|2[0-3])([0-5][0-9])([0-5][0-9])$'
    try:
        if bool(re.match(date_pattern, str(date_magic))):
            if bool(re.match(time_pattern, str(time_magic))):
                return fn_convert_datetime_magic_to_datetime(date_magic, time_magic)
            else:
                return fn_convert_datetime_magic_to_datetime(date_magic, '000000')
        else: 
            return datetime.strptime(value_default, format)
    except:
        return None   
    
def fn_transform_Date_to_String(date, format, value_default): 
    try:
        if not date is None:
            return date.strftime(format)
        else:
            return value_default
    except:
        return None 

def fn_transform_Datetime(value, format, value_default):
    regex_pattern = r"^\d{4}-(0[1-9]|1[0-2])-([0-2][0-9]|3[01]) ([01][0-9]|2[0-3]):([0-5][0-9]):([0-5][0-9])$"
    if value == '': 
        current_value = datetime.strptime(value_default, format)
    else:
        try:
            if bool(re.match(regex_pattern, value)):
                current_value = datetime.strptime(value, format)
            else:
                current_value = datetime.strptime(value_default, format)
        except:
            current_value = datetime.strptime(value_default, format)
    return current_value

def fn_transform_Case(value, exp_case, default = None):
    try:
        list_case = exp_case.split(',') 
        for ele_case in list_case:
            value_case = ele_case.split('->')[0]
            label_case = ele_case.split('->')[1]
            if value in value_case.split('|'):
                return label_case
        return default 
    except:
        return default  
    
def fn_transform_Concatenate(value1, value2, concatenador): 
    if value1 is None or value2 is None:
        return None
    return str(value1).strip() + concatenador + str(value2).strip()

def fn_transform_Concatenate3(value1, value2, value3, concatenador): 
    if value1 is None or value2 is None or value3 is None:
        return None
    return str(value1).strip() + concatenador + str(value2).strip() + concatenador + str(value3).strip() 

def fn_transform_Concatenate4(value1, value2, value3, value4, concatenador):  
    if value1 is None or value2 is None or value3 is None or value4 is None:
        return None
    return str(value1).strip() + concatenador + str(value2).strip() + concatenador + str(value3).strip() + concatenador + str(value4).strip() 

def fn_transform_Concatenate5(value1, value2, value3, value4, value5, concatenador):  
    if value1 is None or value2 is None or value3 is None or value4 is None or value5 is None:
        return None
    return str(value1).strip() + concatenador + str(value2).strip() + concatenador + str(value3).strip() + concatenador + str(value4).strip() + concatenador + str(value5).strip() 

def fn_transform_Concatenate6(value1, value2, value3, value4, value5, value6, concatenador):  
    if value1 is None or value2 is None or value3 is None or value4 is None or value5 is None or value6 is None:
        return None
    return str(value1).strip() + concatenador + str(value2).strip() + concatenador + str(value3).strip() + concatenador + str(value4).strip() + concatenador + str(value5).strip() + concatenador + str(value6).strip() 

def fn_transform_PeriodMagic(value, value_default):
    current_value = value_default
    try:
        if value is None or str(value) == "None":
            current_value = value_default 
        elif int(value) >  3652059:
            current_value = value_default 
        elif int(value) <  693595:
            current_value = value_default
        else:
            current_value = int(value)
    except:
        current_value = value_default 
    return fn_convert_integer_to_datetime(current_value).strftime('%Y%m')

def fn_transform_Period(value1, value2, value_default):
    current_value = str(value_default)
    try:
        if value1 is None or str(value1) == "None":
            current_value = str(value_default)
        else: 
            current_value = str(int(value1) * 100 + int(value2))
    except:
        current_value = value_default 
    return current_value

udf_fn_transform_Replace = udf(fn_transform_Replace, StringType())
udf_fn_transform_ByteMagic = udf(fn_transform_ByteMagic, StringType())
udf_fn_transform_ClearDouble = udf(fn_transform_ClearDouble, DoubleType())
udf_fn_transform_ClearString = udf(fn_transform_ClearString, StringType())
udf_fn_transform_DateMagic = udf(fn_transform_DateMagic, DateType())
udf_fn_transform_DatetimeMagic = udf(fn_transform_DatetimeMagic, TimestampType())
udf_fn_transform_Date_to_String = udf(fn_transform_Date_to_String, StringType())
udf_fn_transform_Datetime = udf(fn_transform_Datetime, TimestampType())
udf_fn_transform_Case = udf(fn_transform_Case, StringType())
udf_fn_transform_Concatenate = udf(fn_transform_Concatenate, StringType())
udf_fn_transform_Concatenate3 = udf(fn_transform_Concatenate3, StringType())
udf_fn_transform_Concatenate4 = udf(fn_transform_Concatenate4, StringType())
udf_fn_transform_Concatenate5 = udf(fn_transform_Concatenate5, StringType())
udf_fn_transform_Concatenate6 = udf(fn_transform_Concatenate6, StringType())
udf_fn_transform_PeriodMagic = udf(fn_transform_PeriodMagic, StringType())
udf_fn_transform_Period = udf(fn_transform_Period, StringType())