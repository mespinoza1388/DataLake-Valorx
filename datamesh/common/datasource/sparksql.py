class sparksql:
    def __init__(self, driver_bd, driver_spark, params):
        self.__driver_bd = driver_bd
        self.__driver_spark = driver_spark
        if 'has_secret_manager' in params and params['has_secret_manager']:
            params = self.__get_connection_string_from_secret_manager(params) 
        self.__params = params 

    def __get_conexion_SQLAlchemy(self): 
        from sqlalchemy.engine import URL
        from sqlalchemy import create_engine  
        server = self.__params['host']
        port = self.__params['port']
        database = self.__params['dbname']
        username = self.__params['username']
        password = self.__params['password'] 
        if self.__driver_bd == "SQLSERVER": 
            connection_url = URL.create(
                'mssql+pyodbc',
                username=username,
                password=password,
                host=server,
                port=port,
                database=database, 
                query=dict(driver='ODBC Driver 17 for SQL Server')) 
            return create_engine(connection_url)
        elif self.__driver_bd == "POSTGRES" or self.__driver_bd == "REDSHIFT":            
            connection_url = URL.create(
                'postgresql+psycopg2',
                username=username,
                password=password,
                host=server,
                port=port,
                database=database) 
            return create_engine(connection_url)

    def __get_connection_string_from_secret_manager(self, con_params): 
        from datamesh.common.aws.secretmanagerclient import secretmanagerclient
        sm_client = secretmanagerclient(con_params['region_name'])
        status, con_params = sm_client.get_secret_value(con_params['secret_manager_name'])  
        if status:
            return con_params
        else:
            raise Exception('secret_manager_name not found')

    def __get_spark_session(self):
        if self.__driver_bd == "SQLSERVER": 
            self.__params['url_jdbc'] = 'jdbc:sqlserver://{}:{};databaseName={}'.format(self.__params['host'], self.__params['port'], self.__params['dbname']) 
            self.__driver = 'com.microsoft.sqlserver.jdbc.SQLServerDriver' 
        if self.__driver_bd == "POSTGRES": 
            self.__params['url_jdbc'] = 'jdbc:postgresql://{}:{}/{}'.format(self.__params['host'], self.__params['port'], self.__params['dbname']) 
        if self.__driver_bd == "REDSHIFT":
            self.__params['url_jdbc'] = 'jdbc:redshift://{}:{}/{}'.format(self.__params['host'], self.__params['port'], self.__params['dbname'])   
        if self.__driver_spark == 'SPARK_GLUE':
            from awsglue.context import GlueContext
            from pyspark.context import SparkContext             
            sc = SparkContext.getOrCreate()
            glueContext = GlueContext(sc) 
            return glueContext.spark_session
        else:
            from pyspark.sql import SparkSession
            return SparkSession.builder.getOrCreate()   
    
    def execute_query_to_dataframe(self, query):
        try:
            if self.__driver_spark == 'PANDAS':
                import pandas as pd
                sqlcommand = self.__get_conexion_SQLAlchemy()  
                df = pd.read_sql_query(query, sqlcommand) 
            else:
                spark = self.__get_spark_session()           
                df = spark.read.format("jdbc").option("url", self.__params['url_jdbc']).option("user", self.__params['username']).option("password", self.__params['password']).option("query", query).load()             
            return True, df
        except Exception as ex: 
            return False, str(ex)
    
    def save_table(self, input_df, table_name, mode):
        try:
            url_jdbc = self.__params['url_jdbc']
            username = self.__params['username']
            password = self.__params['password'] 
            driver = self.__driver
            if self.__driver_spark != 'PANDAS':
                if self.__driver_bd == "SQLSERVER": 
                    input_df.write.format("jdbc").option("url", url_jdbc).option("user", username).option("password", password).option("driver", driver).option("dbtable", table_name).mode(mode).save()  
                if self.__driver_bd == "POSTGRES" or self.__driver_bd == "REDSHIFT":
                    input_df.write.format("jdbc").option("url", url_jdbc).option("user", username).option("password", password).option("dbtable", table_name).mode(mode).save()  
            else:
                raise NotImplementedError('No se ha implementado la función')
            return True, ''
        except Exception as ex:
            return False, str(ex)
    
    @staticmethod
    def save_file(driver_spark, df_input, config):
        try: 
            if driver_spark == 'PANDAS': 
                from datamesh.common.aws.s3client import s3client
                import os 
                import time
                s3_client = s3client() 
                df_input.to_csv(config['path_local_file'], header = config['header'], sep = config['delimiter'], index=None)
                status, response = s3_client.upload_file(config['bucket_s3'], config['path_s3_file'], config['path_local_file'])
                if status:
                    os.remove(config['path_local_file']) 
                    return True, 'Se generó el archivo satisfactoriamente'
                else:
                    return False, response
                #df_input.to_csv(config['fullfilename'], header = config['header'], sep = config['delimiter'], index=None)
            else:  
                df_input.write.format(config['format']).option("sep", config['delimiter']).mode(config['mode']).save(config['path_s3'])
                return True, 'Se generó el archivo satisfactoriamente'
        except Exception as ex: 
            return False, str(ex)

    @staticmethod
    def get_column_max_to_dataframe(driver_spark, df_input, column_name):
        try: 
            if driver_spark == 'PANDAS':
                nro_row = df_input.shape[0]
                if nro_row > 0:
                    max_column = df_input[column_name].max()
                    return True, str(max_column) 
            else:
                import pyspark.sql.functions as F
                nro_row = df_input.count()
                if nro_row > 0:
                    max_column = df_input.agg(F.max(column_name)).collect()[0][0]
                    return True, str(max_column)
            return False, None
        except Exception as ex:
            return False, str(ex)

    @staticmethod
    def union_dataframe(driver_spark, df_A, df_B):
        if driver_spark == 'PANDAS':
            import pandas as pd
            return pd.concat([df_A, df_B]) 
        else:
            return df_A.union(df_B)