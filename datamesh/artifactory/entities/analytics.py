import datamesh.artifactory.config.constants as constants
class analytics():     
    def __init__(self, config_datalake = None):  
        index_col_tables = constants.entities_analytics_index_col_tables
        index_col_columns = constants.entities_analytics_index_col_columns
        file_tables = constants.entities_analytics_tables
        file_columns = constants.entities_analytics_columns
        separator = constants.entities_separador
        header = constants.entities_header
        if not config_datalake is None:  
            if constants.entities_type_source == 'aws_s3':     
                bucket_name = config_datalake['bucket_artifactory']
                path_key = config_datalake['path_artifactory_analytics']  
                status, data = self.__get_data_s3(file_tables, separator, index_col_tables, header, bucket_name, path_key) 
                if status:
                    self.__data_tables = data
                else:
                    raise Exception(data) 

                status, data = self.__get_data_s3(file_columns, separator, index_col_columns, header, bucket_name, path_key) 
                if status:
                    self.__data_columns = data 
                else:
                    raise Exception(data)
        else:
            raise NotImplementedError('Not implement function')

    def __get_data_s3(self, file_name, separator, index_col, header, bucket_name, path_key): 
        from datamesh.common.aws.s3client import s3client
        import pandas as pd
        s3_client = s3client()
        status_code, file_tables = s3_client.get_object(bucket_name, path_key + file_name)  
        if status_code:
            try:
                df = pd.read_csv(file_tables, sep=separator, index_col = index_col, header = header)
                df = df.fillna('')
                df = df.sort_index()
                return True, df
            except Exception as ex:
                return False, str(ex)
        else:
            return False, file_tables
          
    def tables(self, table_type=None, table_name=None, schema_name=None, status='a'): 
        data = [] 
        if not self.__data_tables is None:
            df = self.__data_tables
            for i in df.index: 
                if (str(df['table_type'][i]).upper() == table_type.upper() if table_type is not None else str(df['table_type'][i]).upper()) \
                    and (str(i[1]).upper() == table_name.upper() if table_name is not None else str(i[1]).upper()) \
                    and (str(i[0]).upper() == schema_name.upper() if schema_name is not None else str(i[0]).upper()) \
                    and (str(df['status'][i]).upper() == status.upper() if status is not None else str(df['status'][i]).upper()):
                    data.append({
                        'schema_name' : i[0],
                        'table_name' : i[1], 
                        'table_type' : df['table_type'][i], 
                        'status' : df['status'][i]
                    })
        else:
            raise Exception('error cargando analytics_tables')
        return data 
 
    def columns(self, table_name=None, schema_name=None, status='a'): 
        data = []   
        if not self.__data_columns is None:
            df = self.__data_columns
            for i in df.index: 
                if (str(i[1]).upper() == table_name.upper() if table_name is not None else str(i[1]).upper()) \
                    and (str(i[0]).upper() == schema_name.upper() if schema_name is not None else str(i[0]).upper()) \
                    and (str(df['status'][i]).upper() == status.upper() if status is not None else str(df['status'][i]).upper()):
                    data.append({
                        'schema_name' : i[0],
                        'table_name' : i[1],
                        'column_id' : i[2],
                        'column_name' : df['column_name'][i],
                        'column_type' : df['column_type'][i], 
                        'is_pk_table' : df['is_pk_table'][i],
                        'is_pk_table_source' : df['is_pk_table_source'][i],
                        'flat_source' : df['flat_source'][i],
                        'is_partition' : df['is_partition'][i], 
                        'status' : df['status'][i] 
                    }) 
        else:
            raise Exception('error cargando analytics_columns') 
        return data  
 