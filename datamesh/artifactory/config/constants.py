######### entities 
entities_type_source = 'aws_s3'
entities_separador = ';'
entities_header = 0

entities_raw_tables = 'raw_tables.csv'
entities_raw_columns = 'raw_columns.csv'
entities_raw_index_col_tables = ['schema_group','table_name']
entities_raw_index_col_columns = ['schema_group','table_name','column_id']

entities_stage_tables = 'stage_tables.csv'
entities_stage_columns = 'stage_columns.csv'
entities_stage_index_col_tables = ['schema_group','table_name']
entities_stage_index_col_columns = ['schema_group','table_name','column_id']

entities_analytics_tables = 'analytics_tables.csv'
entities_analytics_columns = 'analytics_columns.csv'
entities_analytics_index_col_tables = ['schema_name','table_name']
entities_analytics_index_col_columns = ['schema_name','table_name','column_id']

######### tables dynamodb 
dynamodb_tables_audit_prefix = 'dm' #'aje'
dynamodb_tables_config_prefix = 'dm' #'aje'