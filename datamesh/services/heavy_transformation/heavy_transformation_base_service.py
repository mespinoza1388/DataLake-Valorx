from datamesh.services.base_service import base_service 
from datamesh.artifactory.entities.stage import stage
from datamesh.artifactory.entities.analytics import analytics
class heavy_transformation_base_service(base_service):
    def __init__(self, environment, schemas, config = None): 
        super().__init__(environment, schemas, config)    
        self.__data_config = self.get_config()  
        obj_stage = stage(self.__data_config['datalake'])
        obj_analytics = analytics(self.__data_config['datalake'])
        self.__data_entity = {}
        self.__data_entity['analytics_tables'] = obj_analytics.tables()
        self.__data_entity['analytics_columns'] = obj_analytics.columns()
        self.__data_entity['stage_tables'] = obj_stage.tables()
        self.__data_entity['stage_columns'] = obj_stage.columns()
     
    def get_analytics_tables(self, table_type=None, table_name=None, schema_name=None, status='a'):
        data = []
        analytics_tables = self.__data_entity['analytics_tables']
        for analytics_table in analytics_tables:
            if (analytics_table['table_type'].upper() == table_type.upper() if table_type is not None else analytics_table['table_type'].upper()) \
                and (analytics_table['table_name'].upper() == table_name.upper() if table_name is not None else analytics_table['table_name'].upper()) \
                and (analytics_table['schema_name'].upper() == schema_name.upper() if schema_name is not None else analytics_table['schema_name'].upper()) \
                and (analytics_table['status'].upper() == status.upper() if status is not None else analytics_table['status'].upper()):
                    data.append(analytics_table)
        return data 
    
    def get_analytics_columns(self, table_name=None, schema_name=None, status='a'):
        data = []
        analytics_columns = self.__data_entity['analytics_columns']
        for analytics_column in analytics_columns:
            if (analytics_column['table_name'].upper() == table_name.upper() if table_name is not None else analytics_column['table_name'].upper()) \
                and (analytics_column['schema_name'].upper() == schema_name.upper() if schema_name is not None else analytics_column['schema_name'].upper()) \
                and (analytics_column['status'].upper() == status.upper() if status is not None else analytics_column['status'].upper()):
                data.append(analytics_column)
        return data 
    
    def get_stage_tables(self, table_type=None, table_name=None, schema_group=None, status='a'): 
        data = []
        stage_tables = self.__data_entity['stage_tables'] 
        for stage_table in stage_tables:
            if (stage_table['table_type'].upper() == table_type.upper() if table_type is not None else stage_table['table_type'].upper()) \
                and (stage_table['table_name'].upper() == table_name.upper() if table_name is not None else stage_table['table_name'].upper()) \
                and (stage_table['schema_group'].upper() == schema_group.upper() if schema_group is not None else stage_table['schema_group'].upper()) \
                and (stage_table['status'].upper() == status.upper() if status is not None else stage_table['status'].upper()):
                data.append(stage_table)
        return data 
    
    def get_stage_columns(self, table_name=None, schema_group=None, status='a'):
        data = []
        stage_columns = self.__data_entity['stage_columns']
        for stage_column in stage_columns:
            if (stage_column['table_name'].upper() == table_name.upper() if table_name is not None else stage_column['table_name'].upper()) \
                and (stage_column['schema_group'].upper() == schema_group.upper() if schema_group is not None else stage_column['schema_group'].upper()) \
                and (stage_column['status'].upper() == status.upper() if status is not None else stage_column['status'].upper()):
                data.append(stage_column)
        return data 
  
    def get_script_drop_table_analytics(self, stage_table):
        scripts = []
        scripts.append('DROP TABLE IF EXISTS ' + self.get_schema_analytics_athena() + '.' + stage_table['table_name'] ) 
        return scripts
    
    def get_script_create_table_analytics_dim(self, analytics_table, analytics_columns):
        scripts = []  
        analytics_columns_partition = []
        analytics_columns_nonpartition = []   
        for analytics_column in analytics_columns:
            if analytics_column['is_partition'] == 't':
                analytics_columns_partition.append(analytics_column['column_name'] + ' ' + analytics_column['column_type'])
            else:
                analytics_columns_nonpartition.append(analytics_column['column_name'] + ' ' + analytics_column['column_type'])

        script_nonpartition = "(" + " ,".join(analytics_columns_nonpartition) + ")"   
        script_partition = "" 
        if len(analytics_columns_partition) > 0:
            script_partition = "PARTITIONED BY (" + " ,".join(analytics_columns_partition) + ")"  
             
        scripts.append('CREATE EXTERNAL TABLE IF NOT EXISTS ' + self.get_schema_analytics_athena() + '.' + analytics_table['table_name'])         
        scripts.append(script_nonpartition)  
        scripts.append(script_partition)
        scripts.append('ROW FORMAT SERDE "org.apache.hadoop.hive.ql.io.parquet.serde.ParquetHiveSerDe"')
        scripts.append('STORED AS INPUTFORMAT "org.apache.hadoop.hive.ql.io.SymlinkTextInputFormat"')
        scripts.append('OUTPUTFORMAT "org.apache.hadoop.hive.ql.io.HiveIgnoreKeyTextOutputFormat"')
        scripts.append('LOCATION "' + self.get_path_full_analytics_s3() + '/' + analytics_table['table_name'] + \
            '/_symlink_format_manifest/";')
        return scripts

    def get_script_create_table_analytics_fact(self, analytics_table, analytics_columns):
        scripts = [] 
        analytics_columns_partition = []
        analytics_columns_nonpartition = []
        for analytics_column in analytics_columns:
            if analytics_column['is_partition'] == 't':
                analytics_columns_partition.append(analytics_column['column_name'] + ' ' + analytics_column['column_type'])
            else:
                analytics_columns_nonpartition.append(analytics_column['column_name'] + ' ' + analytics_column['column_type'])
        
        script_nonpartition = "(" + " ,".join(analytics_columns_nonpartition) + ")"   
        script_partition = "" 
        if len(analytics_columns_partition) > 0:
            script_partition = "PARTITIONED BY (" + " ,".join(analytics_columns_partition) + ")"   
             
        scripts.append('CREATE EXTERNAL TABLE IF NOT EXISTS ' + self.get_schema_analytics_athena() + '.' + analytics_table['table_name'])         
        scripts.append(script_nonpartition) 
        scripts.append(script_partition)
        scripts.append('ROW FORMAT SERDE "org.apache.hadoop.hive.ql.io.parquet.serde.ParquetHiveSerDe"')
        scripts.append('STORED AS INPUTFORMAT "org.apache.hadoop.hive.ql.io.SymlinkTextInputFormat"')
        scripts.append('OUTPUTFORMAT "org.apache.hadoop.hive.ql.io.HiveIgnoreKeyTextOutputFormat"')
        scripts.append('LOCATION "' + self.get_path_full_analytics_s3() + '/' + analytics_table['table_name'] + \
            '/_symlink_format_manifest/";')
        return scripts
    
    def get_script_repair_table_analytics_fact(self, analytics_table):
        scripts = []
        scripts.append('MSCK REPAIR TABLE ' + self.get_schema_analytics_athena() + '.' + analytics_table['table_name'] ) 
        return scripts
     
    def get_schema_stage_athena(self):
        return self.__data_config['schema']['team'] + '_' \
            + self.__data_config['schema']['datasource'] + '_' + self.__data_config['schema']['schema'] + '_stage'
    
    def get_path_full_stage_s3(self):
        return 's3://' + self.__data_config['datalake']['bucket_stage'] + '/' \
            + self.__data_config['schema']['team'] + '/' + self.__data_config['schema']['datasource'] + '/' + self.__data_config['schema']['schema'] 
    
    def get_path_stage_s3(self):
        return self.__data_config['schema']['team'] + '/' + self.__data_config['schema']['datasource'] + '/' + self.__data_config['schema']['schema']
    
    def get_schema_analytics_athena(self):
        return self.__data_config['schema']['team'] + '_' + self.__data_config['schema']['schema_analytics']  + '_analytics' 
    
    def get_path_full_analytics_s3(self):
        return 's3://' + self.__data_config['datalake']['bucket_analytics'] + '/' \
                + self.__data_config['schema']['team'] + '/' + self.__data_config['schema']['schema_analytics'] 
    
    def get_path_analytics_s3(self):
        return self.__data_config['schema']['team'] + '/' + self.__data_config['schema']['schema_analytics']
    

    
        
