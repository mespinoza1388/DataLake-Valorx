import logging
import os

class FileLogger(logging.Logger):
    def __init__(self, name, process_id, directory = None): 
        super().__init__(name, level= logging.DEBUG)  
        if not directory is None:
            directory = directory + '/' 
            if not os.path.exists(directory):
                os.mkdir(directory)
        else:
            directory = ''        
        log_filename = directory + 'log_' + process_id + '.log' 
        self.__file_handler = logging.FileHandler(log_filename, mode = 'w')
        self.__file_handler.setLevel(logging.DEBUG)
        formatter = logging.Formatter('%(asctime)s|%(lineno)s|%(levelname)s|%(message)s')
        self.__file_handler.setFormatter(formatter) 
        super().addHandler(self.__file_handler)
        
    def close(self): 
        self.__file_handler.close()         
        super().removeHandler(self.__file_handler)
        logging.shutdown()
