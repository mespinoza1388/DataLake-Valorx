from datamesh.services.base_service import base_service
from datamesh.artifactory.entities.raw import raw 

class base_extractor_service(base_service):     
    def __init__(self, environment, schemas, config = None): 
        super().__init__(environment, schemas, config)  
        self.__data_config = self.get_config() 
        obj_raw = raw(self.get_config_datalake())
        self.__data_entity = {}
        self.__data_entity['raw_tables'] = obj_raw.tables()
        self.__data_entity['raw_columns'] = obj_raw.columns()
    
    def get_raw_entities(self, type_process=None, table_type=None, table_name=None, schema_group=None, table_priority =None, process_id=None, status='a'):
        raw_tables = self.get_raw_tables(table_type = table_type, table_name = table_name, schema_group = schema_group, table_priority = table_priority, process_id = process_id)
        columns = ''
        data = []
        for raw_table in raw_tables:
            raw_columns = self.get_raw_columns(table_name = raw_table['table_name'], schema_group = raw_table['schema_group'])
            columns = ''
            for raw_column in raw_columns: 
                if raw_column['is_column_calculated'] == 't':
                    columns = columns + raw_column['exp_column_calculated'] + ' as ' + raw_column['column_name'] + ','
                else:
                    columns = columns + '[' +  raw_column['column_name'] + ']' + ','
            columns = columns[:-1]  
            data.append({  
                'schema_group' : raw_table['schema_group'],
                'table_name' : raw_table['table_name'],
                'table_type' : raw_table['table_type'],
                'table_alias' : raw_table['table_alias'],  
                'table_priority' : raw_table['table_priority'],  
                'process_id' : raw_table['process_id'],  
                'column_pk' : raw_table['column_pk'],
                'column_lastmodifydate' : raw_table['column_lastmodifydate'],
                'has_join' : raw_table['has_join'],  
                'exp_join' : raw_table['exp_join'],  
                'has_filter' : raw_table['has_filter'],
                'exp_filter' : raw_table['exp_filter'],  
                'type_filter_full' : raw_table['type_filter_full'],
                'exp_filter_full' : raw_table['exp_filter_full'], 
                'type_filter_incremental' : raw_table['type_filter_incremental'],
                'exp_filter_incremental' : raw_table['exp_filter_incremental'],    
                'delay_incremental_ini' : raw_table['delay_incremental_ini'],   
                'delay_incremental_end' : raw_table['delay_incremental_end'],             
                'type_limit_period' : raw_table['type_limit_period'],               
                'query_limit_period' : raw_table['query_limit_period'], 
                'status' : raw_table['status'],
                'columns' : columns,  
                'type_process' : type_process,
                'val_filter_full' : '',
                'val_filter_incremental' : '',
            }) 
        return data
    
    def get_raw_tables(self, table_type=None, table_name=None, schema_group=None, table_priority=None, process_id=None, status='a'):
        data = []
        raw_tables = self.__data_entity['raw_tables']
        for raw_table in raw_tables:
            if (raw_table['table_type'].upper() == table_type.upper() if table_type is not None else raw_table['table_type'].upper()) \
                and (raw_table['table_name'].upper() == table_name.upper() if table_name is not None else raw_table['table_name'].upper()) \
                and (raw_table['schema_group'].upper() == schema_group.upper() if schema_group is not None else raw_table['schema_group'].upper()) \
                and (str(raw_table['table_priority']) == str(table_priority) if table_priority is not None else str(raw_table['table_priority'])) \
                and (str(raw_table['process_id']) == str(process_id)if process_id is not None else str(raw_table['process_id'])) \
                and (raw_table['status'].upper() == status.upper() if status is not None else raw_table['status'].upper()):
                data.append(raw_table)
        return data 
    
    def get_raw_columns(self, table_name=None, schema_group=None, status='a'):
        data = []
        raw_columns = self.__data_entity['raw_columns']
        for raw_column in raw_columns:
            if (raw_column['table_name'].upper() == table_name.upper() if table_name is not None else raw_column['table_name'].upper()) \
                and (raw_column['schema_group'].upper() == schema_group.upper() if schema_group is not None else raw_column['schema_group'].upper()) \
                and (raw_column['status'].upper() == status.upper() if status is not None else raw_column['status'].upper()):
                data.append(raw_column)
        return data 
      
    def get_schema_raw_athena(self):
        return self.__data_config['schema']['team'] + '_' \
            + self.__data_config['schema']['datasource'] + '_' + self.__data_config['schema']['schema'] + '_raw'
    
    def get_path_full_raw_s3(self):
        return 's3://' + self.__data_config['datalake']['bucket_raw'] + '/' \
            + self.__data_config['schema']['team'] + '/' + self.__data_config['schema']['datasource'] + '/' + self.__data_config['schema']['schema'] 
    
    def get_path_raw_s3(self):
        return self.__data_config['schema']['team'] + '/' + self.__data_config['schema']['datasource'] + '/' + self.__data_config['schema']['schema']