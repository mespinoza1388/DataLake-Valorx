from datamesh.common.aws.dynamodbresource import dynamodbresource
import datamesh.artifactory.config.constants as constants
class configtabledb(): 
    def __init__(self, environment, schemas, config = None):     
        self.__prefix = constants.dynamodb_tables_config_prefix
        if 'schema_id' in schemas:
            self.__schema_id = schemas['schema_id'] 
        self.__dynamodb = dynamodbresource()
        self.__environment = environment 

    def get_config_table(self, table_id):
        config_table = {}
        key_input = {
            'table_id': table_id,
            'schema_id': self.__schema_id
            }
        status, response = self.__dynamodb.get_item(self.__prefix +  '_' + self.__environment + '_datalake_config_table', key_input)
        if status:
            config_table['table_id'] = table_id
            config_table['schema_id'] = self.__schema_id
            config_table['lastprocess_guid'] = response['lastprocess_guid']
            config_table['lastmodifydate'] = response['lastmodifydate']        
        return status, config_table

    def put_config_table(self, table_id, lastmodifydate, lastprocess_guid):
        item_input = {
            'table_id': table_id,
            'schema_id': self.__schema_id,
            'lastmodifydate': lastmodifydate,
            'lastprocess_guid': lastprocess_guid
            }
        return self.__dynamodb.put_item(self.__prefix +  '_' + self.__environment + '_datalake_config_table', item_input)
        