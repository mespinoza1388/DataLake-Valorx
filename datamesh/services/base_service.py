from datamesh.artifactory.config.configdb import configdb
from datamesh.artifactory.config.configtabledb import configtabledb 

class base_service:     
    def __init__(self, environment, schemas, config = None):   
        obj_configdb = configdb(environment, schemas, config)
        self.__data_config = {}
        self.__data_config['schema'] = obj_configdb.config_schema()
        self.__data_config['datalake'] = obj_configdb.config_datalake()
        self.__obj_configtabledb = configtabledb(environment, schemas, config) 

    def get_config(self):
        return self.__data_config
    
    def get_config_schema(self):
        return self.__data_config['schema']
    
    def get_config_datalake(self):
        return self.__data_config['datalake']
           
    def get_config_table_audit(self, table_id):
        if self.__obj_configtabledb is None:
            return False, 'No se inicializó configtabledb'
        return self.__obj_configtabledb.get_config_table(table_id)
        
    def put_config_table_audit(self, table_id, lastmodifydate, lastprocess_guid):
        if self.__obj_configtabledb is None:
            return False, 'No se inicializó configtabledb'
        return self.__obj_configtabledb.put_config_table(table_id, lastmodifydate, lastprocess_guid)
  