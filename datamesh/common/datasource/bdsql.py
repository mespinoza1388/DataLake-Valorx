class bdsql:
    def __init__(self, driver, params):
        self.__driver = driver 
        if 'has_secret_manager' in params and params['has_secret_manager']:
            params = self.__get_connection_string_from_secret_manager(params) 
        self.__params = params  

    def __get_connection_string_from_secret_manager(self, con_params): 
        from datamesh.common.aws.secretmanagerclient import secretmanagerclient
        sm_client = secretmanagerclient(con_params['region_name'])
        status, con_params = sm_client.get_secret_value(con_params['secret_manager_name'])  
        if status:
            return con_params
        else:
            raise Exception('secret_manager_name not found')
    
    def __get_conexion(self):  
        server = self.__params['host']
        port = self.__params['port']
        database = self.__params['dbname']
        username = self.__params['username']
        password = self.__params['password'] 
        if self.__driver == "SQLSERVER": 
            import pyodbc
            return pyodbc.connect(
                'DRIVER={SQL Server};'
                      f'SERVER={server},{port};'
                      f'DATABASE={database};'
                      f'UID={username};'
                      f'PWD={password}', timeout=30
            )
        elif self.__driver == "POSTGRES" or self.__driver == "REDSHIFT":            
            import psycopg2
            return psycopg2.connect(
                host=server,
                port=port,
                dbname=database,
                user=username,
                password=password
            )  

    def execute_nonquery(self, query):
        try: 
            sqlcommand = self.__get_conexion()
            if self.__driver == "POSTGRES":
                sqlcommand.autocommit = True
            cursor = sqlcommand.cursor() 
            cursor.execute(query) 
            if self.__driver == "POSTGRES":
                sqlcommand.commit()
            sqlcommand.close()
            return True, ''
        except Exception as ex: 
            return False, str(ex)
        
    def execute_query_to_list(self, query):
        try: 
            sqlcommand = self.__get_conexion() 
            if self.__driver == "POSTGRES":
                sqlcommand.autocommit = True
            cursor = sqlcommand.cursor() 
            cursor.execute(query) 
            results = cursor.fetchall()
            if self.__driver == "POSTGRES":
                sqlcommand.commit()
            sqlcommand.close()
            return True, results
        except Exception as ex: 
            return False, str(ex)
 