from datamesh.common.aws.base import base
class athenaclient(base):
    def __init__(self, config = None):
         super().__init__(config)
         self.__client_athena = self.session.client('athena')

    def start_query_execution(self, query, database, backet_output):
        try: 
            response = self.__client_athena.start_query_execution( 
                QueryString= query,
                QueryExecutionContext = {
                        'Database': database
                    }, 
                ResultConfiguration = { 'OutputLocation': backet_output}
                ) 
            return True, response['QueryExecutionId']
        except Exception as ex:
            return False, str(ex)

    def get_query_execution(self, query_execute_id):
        try: 
            response = self.__client_athena.get_query_execution(QueryExecutionId = query_execute_id) 
            if response['ResponseMetadata']['HTTPStatusCode'] == 200:
                return True, {
                    'QueryExecution' : response['QueryExecution'],
                    'Status' : response['Status']
                    }
            else:
                return False, 'No se encontró la ejecución'
        except Exception as ex:
            return False, str(ex)   