from datamesh.common.aws.base import base
class dynamodbresource(base):
    def __init__(self, config = None):
         super().__init__(config)
         self.__resource_dynamodb = self.session.resource('dynamodb')
         
    def get_item_by_key(self, table_name, key_name, key_value):
        try: 
            table = self.__resource_dynamodb.Table(table_name)
            response = table.get_item(
                Key={
                    key_name: key_value
                }
            )
            return True, response['Item']
        except Exception as e:
            return False, str(e)

    def get_item_by_key_and_sortkey(self, table_name, key_name, key_value, key_sort_name, key_sort_value):
        try: 
            table = self.__resource_dynamodb.Table(table_name)
            response = table.get_item(
                Key={
                    key_name: key_value,
                    key_sort_name: key_sort_value
                }
            )
            return True, response['Item']
        except Exception as e:
            return False, str(e)        

    def get_item(self, table_name, key_input):
        try: 
            db_table = self.__resource_dynamodb.Table(table_name)
            response = db_table.get_item(
                Key=key_input
            )
            return True, response['Item']
        except Exception as e:
            return False, str(e)

    def put_item(self, table_name, item_input):
        try: 
            db_table = self.__resource_dynamodb.Table(table_name)
            response = db_table.put_item(
                Item = item_input
            )
            status_code = response.get("ResponseMetadata", {}).get("HTTPStatusCode")
            if status_code == 200:
                return True, 'Se insertó/actualizó el registro'
            else:
                return False, 'No se realizaron los cambios de los registros' 
        except Exception as e:
            return False, str(e)   

    def upd_item_with_expression(self, table_name, key_input, upd_expression, exp_attribute_names, exp_attribute_values, return_values): 
        try: 
            db_table = self.__resource_dynamodb.Table(table_name) 
            response = db_table.update_item(Key = key_input
                                    ,UpdateExpression=upd_expression
                                    ,ExpressionAttributeValues=exp_attribute_values
                                    ,ExpressionAttributeNames=exp_attribute_names
                                    ,ReturnValues=return_values) 
            status_code = response.get("ResponseMetadata", {}).get("HTTPStatusCode")
            if status_code == 200:
                return True, 'Se insertó/actualizó el registro'
            else:
                return False, 'No se realizaron los cambios de los registros' 
        except Exception as e:
            return False, str(e)

    def upd_item(self, table_name, key_input, upd_expression, exp_attribute_values, return_values): 
        try: 
            db_table = self.__resource_dynamodb.Table(table_name) 
            response = db_table.update_item(Key = key_input
                                    ,UpdateExpression=upd_expression 
                                    ,ExpressionAttributeValues=exp_attribute_values
                                    ,ReturnValues=return_values) 
            status_code = response.get("ResponseMetadata", {}).get("HTTPStatusCode")
            if status_code == 200:
                return True, 'Se insertó/actualizó el registro'
            else:
                return False, 'No se realizaron los cambios de los registros' 
        except Exception as e:
            return False, str(e)