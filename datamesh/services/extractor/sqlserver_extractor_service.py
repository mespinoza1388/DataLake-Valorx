from datamesh.services.extractor.base_extractor_service import base_extractor_service  
class sqlserver_extractor_service(base_extractor_service):    
    def __init__(self, environment, schemas, config = None): 
        super().__init__(environment, schemas, config)    

    def get_script_create_table_raw(self, raw_table, raw_columns):
        scripts = []
        raw_columns_nonpartition = []
        for raw_column in raw_columns:
            raw_columns_nonpartition.append(raw_column['column_name'] + ' ' + raw_column['column_type'])
        script_nonpartition = "(" + " ,".join(raw_columns_nonpartition) + ")" 
        scripts.append('CREATE EXTERNAL TABLE IF NOT EXISTS ' + super().get_schema_raw_athena() + '.' + raw_table['table_name']) 
        scripts.append(script_nonpartition) 
        scripts.append('ROW FORMAT SERDE "org.apache.hadoop.hive.serde2.OpenCSVSerde"')
        scripts.append('STORED AS INPUTFORMAT "org.apache.hadoop.mapred.TextInputFormat"')
        scripts.append('OUTPUTFORMAT "org.apache.hadoop.hive.ql.io.HiveIgnoreKeyTextOutputFormat"')
        scripts.append('LOCATION "' + super().get_path_full_raw_s3() + '/' + raw_table['table_name'] + '/"')
        scripts.append('TBLPROPERTIES ("separatorChar"="|");')
        return scripts     

    def get_custom_query(self, entity, type_process, date_period = None): 
        columns = entity['columns']
        table_name = entity['table_name'] if entity['table_alias'] == '' else entity['table_alias']
        join_table = ' ' + entity['exp_join'] if entity['has_join'].upper() == 'T' else ''
        filter_initial = entity['exp_filter'] if entity['has_filter'].upper() == 'T' else ''
        filter_full = entity['exp_filter_full'] if entity['type_filter_full'] != '' else ''
        filter_incremental = entity['exp_filter_incremental'] if entity['type_filter_incremental'] != '' else ''
            
        filters = []
        if filter_initial != '':
            filters.append(filter_initial)
        if filter_full != '' and type_process == 'F' and not date_period is None:
            filter_full = filter_full.replace('{0}', str(date_period[0])).replace('{1}', str(date_period[1]))
            filters.append(filter_full)            
        if filter_incremental != '' and type_process == 'I' and not date_period is None:
            filter_incremental = filter_incremental.replace('{0}', str(date_period[0])).replace('{1}', str(date_period[1]))
            filters.append(filter_incremental)
            
        filter = ' AND '.join(filters)
        filter = ('WHERE ' + filter if filter != '' else '')

        return 'SET DATEFORMAT DMY; SELECT ' + columns + ' FROM ' + table_name + join_table + ' ' + filter

    def get_custom_query_test(self, entity, type_process, date_period = None): 
        columns = entity['columns']
        table_name = entity['table_name'] if entity['table_alias'] == '' else entity['table_alias']
        join_table = ' ' + entity['exp_join'] if entity['has_join'].upper() == 'T' else ''
        filter_initial = entity['exp_filter'] if entity['has_filter'].upper() == 'T' else ''
        filter_full = entity['exp_filter_full'] if entity['type_filter_full'] != '' else ''
        filter_incremental = entity['exp_filter_incremental'] if entity['type_filter_incremental'] != '' else ''
            
        filters = []
        if filter_initial != '':
            filters.append(filter_initial)
        if filter_full != '' and type_process == 'F' and not date_period is None:
            filter_full = filter_full.replace('{0}', str(date_period[0])).replace('{1}', str(date_period[1]))
            filters.append(filter_full)
        if filter_incremental != '' and type_process == 'I' and not date_period is None:
            filter_incremental = filter_incremental.replace('{0}', str(date_period[0])).replace('{1}', str(date_period[1]))
            filters.append(filter_incremental)
      
        filter = ' AND '.join(filters)
        filter = ('WHERE ' + filter if filter != '' else '')

        return 'SET DATEFORMAT DMY; SELECT TOP 2 ' + columns + ' FROM ' + table_name + join_table + ' ' + filter
    
    def get_script_drop_table_raw(self, raw_table):
        scripts = []
        scripts.append('DROP TABLE IF EXISTS ' + super().get_schema_raw_athena() + '.' + raw_table['table_name'] ) 
        return scripts
    
    