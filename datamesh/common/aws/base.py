import boto3
import os
class base:
    def __init__(self, config = None):
        if config is None:
            self.session = boto3._get_default_session()
        else:
            if 'boto3_session' in config:
                self.session = config['boto3_session']
            else:
                self.session = boto3._get_default_session()