from datamesh.services.extractor.base_extractor_service import base_extractor_service  

class oracle_extractor_service(base_extractor_service):    
    def __init__(self, environment, schemas, config = None): 
        super().__init__(environment, schemas, config)    
    
    def get_script_create_table_raw(self, raw_table, raw_columns):
        scripts = []
        raw_columns_nonpartition = []
        for raw_column in raw_columns:
            raw_columns_nonpartition.append(raw_column['column_name'] + ' ' + (raw_column['column_type'] + ' COMMENT "from deserializer" ' if raw_column['column_type'] == 'string' else raw_column['column_type']))
        script_nonpartition = "(" + " ,".join(raw_columns_nonpartition) + ")" 
        
        scripts.append('CREATE EXTERNAL TABLE IF NOT EXISTS ' + super().get_schema_raw_athena() + '.' + raw_table['table_name']) 
        scripts.append(script_nonpartition) 
        scripts.append('ROW FORMAT SERDE "org.apache.hadoop.hive.serde2.OpenCSVSerde"')
        scripts.append('STORED AS INPUTFORMAT "org.apache.hadoop.mapred.TextInputFormat"')
        scripts.append('OUTPUTFORMAT "org.apache.hadoop.hive.ql.io.HiveIgnoreKeyTextOutputFormat"')
        scripts.append('LOCATION "' + super().get_path_full_raw_s3() + '/' + raw_table['table_name'] + '/"')
        scripts.append('TBLPROPERTIES ("compressiontype"="gz", "skip.header.line.count"="1");')
        return scripts    

    def get_script_drop_table_raw(self, raw_table):
        scripts = []
        scripts.append('DROP TABLE IF EXISTS ' + super().get_schema_raw_athena() + '.' + raw_table['table_name'] ) 
        return scripts
    
    def get_script_create_table_raw_redshift(self, schema_redshift, raw_table, raw_columns):
        scripts = []
        raw_columns_nonpartition = []
        for raw_column in raw_columns:
            raw_columns_nonpartition.append(raw_column['column_name'] + ' ' + (' VARCHAR(500) ENCODE lzo ' if raw_column['column_type'] == 'string' else raw_column['column_type']))
        script_nonpartition = "(" + " ,".join(raw_columns_nonpartition) + ")" 
        
        scripts.append('CREATE TABLE IF NOT EXISTS ' + schema_redshift + '.' + raw_table['table_alias']) 
        scripts.append(script_nonpartition) 
        scripts.append('DISTSTYLE AUTO;') 
        scripts.append('ALTER TABLE '+ schema_redshift + '.' + raw_table['table_alias'] + ' owner to dwhuser;') 
        return scripts    
 
    def get_script_create_function_raw_redshift(self, schema_redshift_source, schema_redshift_destinate, raw_table, raw_columns):
        scripts = []
        raw_columns_select = []
        raw_columns_insert = []
        raw_columns_on = []
        raw_columns_where = []
        raw_columns_pk = []
        
        raw_columns_update = [] 
        for raw_column in raw_columns:
            raw_columns_select.append(raw_column['column_name'])
            raw_columns_insert.append('t.' + raw_column['column_name']) 
            raw_columns_update.append(raw_column['column_name'] + ' = ' + 't.' + raw_column['column_name'])
        
        script_select = " ,".join(raw_columns_select) 
        script_insert = " ,".join(raw_columns_insert) 
        script_update = " ,".join(raw_columns_update) 

        column_pk = raw_table['column_pk']  
        raw_columns_pk = column_pk.split(',') 

        for raw_column_pk in raw_columns_pk: 
            raw_columns_on.append('T.' + raw_column_pk.strip() + ' = O.' + raw_column_pk.strip())  
            raw_columns_where.append('O.' + raw_column_pk.strip() + ' IS NULL') 

        script_where = " AND ".join(raw_columns_where)
        script_on = " AND ".join(raw_columns_on)


        scripts.append('CREATE OR REPLACE PROCEDURE  ' + schema_redshift_source + '.usp_load_' + raw_table['table_alias'] + '()')
        scripts.append('\n') 
        scripts.append('LANGUAGE plpgsql')
        scripts.append('\n') 
        scripts.append('AS $$')
        scripts.append('\n') 
        scripts.append('BEGIN')
        scripts.append('\n') 
        scripts.append('SELECT ' + script_select + ' ') 
        scripts.append('\n') 
        scripts.append('INTO #' + raw_table['table_alias']) 
        scripts.append('\n')  
        scripts.append('FROM ( ') 
        scripts.append('\n') 
        scripts.append('SELECT ROW_NUMBER() OVER (PARTITION BY ' + raw_table['column_pk'] + ' ORDER BY ' + raw_table['column_lastmodifydate'] + ') orden, ')
        scripts.append('\n') 
        scripts.append(script_select + ' ') 
        scripts.append('\n') 
        scripts.append('FROM ' + schema_redshift_source  + '.' +  raw_table['table_alias'] + ') T') 
        scripts.append('\n') 
        scripts.append('WHERE T.orden = 1;') 
        scripts.append('\n')
        scripts.append('INSERT INTO ' + schema_redshift_destinate + '.' + raw_table['table_alias'])
        scripts.append('\n')  
        scripts.append('(' + script_select + ')') 
        scripts.append('\n') 
        scripts.append('SELECT ' + script_insert + ' ')
        scripts.append('\n') 
        scripts.append('FROM #' + raw_table['table_alias'] + ' T') 
        scripts.append('\n') 
        scripts.append('LEFT JOIN ' + schema_redshift_destinate + '.' + raw_table['table_alias'] + ' O ON ' + script_on) 
        scripts.append('\n') 
        scripts.append('WHERE ' + script_where + ';') 
        scripts.append('\n')  
        scripts.append('UPDATE ' + schema_redshift_destinate + '.' + raw_table['table_alias'] + ' SET ')
        scripts.append('\n') 
        scripts.append(script_update) 
        scripts.append('\n') 
        scripts.append('FROM (#' + raw_table['table_alias'] + ' T') 
        scripts.append('\n') 
        scripts.append('INNER JOIN ' + schema_redshift_destinate + '.' + raw_table['table_alias'] + ' O ON ' + script_on + ');') 
        scripts.append('\n')
        scripts.append('END;')
        scripts.append('\n')
        scripts.append('$$;')
        scripts.append('\n')
         
        return scripts 
    
    def get_script_create_function_initial_raw_redshift(self, schema_redshift_source, schema_redshift_destinate, raw_table, raw_columns):
        scripts = []
        raw_columns_select = []
        raw_columns_insert = [] 
        
        raw_columns_update = [] 
        for raw_column in raw_columns:
            raw_columns_select.append(raw_column['column_name'])
            raw_columns_insert.append('t.' + raw_column['column_name']) 
            raw_columns_update.append(raw_column['column_name'] + ' = ' + 't.' + raw_column['column_name'])
        
        script_select = " ,".join(raw_columns_select)  
  
        scripts.append('CREATE OR REPLACE PROCEDURE  ' + schema_redshift_source + '.usp_load_initial_' + raw_table['table_alias'] + '()')
        scripts.append('\n') 
        scripts.append('LANGUAGE plpgsql')
        scripts.append('\n') 
        scripts.append('AS $$')
        scripts.append('\n') 
        scripts.append('BEGIN')
        scripts.append('\n') 
        scripts.append('TRUNCATE TABLE ' + schema_redshift_destinate + '.' + raw_table['table_alias'] + ';')
        scripts.append('\n') 
        scripts.append('INSERT INTO ' + schema_redshift_destinate + '.' + raw_table['table_alias'])
        scripts.append('\n')
        scripts.append('SELECT ' + script_select)
        scripts.append('\n')
        scripts.append('FROM ( ')
        scripts.append('SELECT ROW_NUMBER() OVER (PARTITION BY ' + raw_table['column_pk'] + ' ORDER BY ' + raw_table['column_lastmodifydate'] + ') orden, ')
        scripts.append('\n') 
        scripts.append(script_select) 
        scripts.append('\n')  
        scripts.append('FROM ' + schema_redshift_source + '.' + raw_table['table_alias']) 
        scripts.append('\n')
        scripts.append(') t WHERE t.orden = 1;') 
        scripts.append('\n')
        scripts.append('END;')
        scripts.append('\n')
        scripts.append('$$;')
        scripts.append('\n')
         
        return scripts 
    
    def get_script_create_function_deleted_duplicated_raw_redshift(self, schema_redshift_destinate, raw_table, raw_columns):
        scripts = []
        raw_columns_select = []
        raw_columns_insert = [] 
        
        raw_columns_update = [] 
        for raw_column in raw_columns:
            raw_columns_select.append(raw_column['column_name'])
            raw_columns_insert.append('t.' + raw_column['column_name']) 
            raw_columns_update.append(raw_column['column_name'] + ' = ' + 't.' + raw_column['column_name'])
        
        script_select = " ,".join(raw_columns_select) 
 
        scripts.append('CREATE OR REPLACE PROCEDURE  ' + schema_redshift_destinate + '.usp_deleted_duplicated_' + raw_table['table_alias'] + '()')
        scripts.append('\n') 
        scripts.append('LANGUAGE plpgsql')
        scripts.append('\n') 
        scripts.append('AS $$')
        scripts.append('\n') 
        scripts.append('BEGIN')  
        scripts.append('\n')
        scripts.append('SELECT ' + script_select)
        scripts.append('\n')
        scripts.append('INTO #' + raw_table['table_alias']) 
        scripts.append('\n')
        scripts.append('FROM ( ')
        scripts.append('SELECT ROW_NUMBER() OVER (PARTITION BY ' + raw_table['column_pk'] + ' ORDER BY ' + raw_table['column_lastmodifydate'] + ') orden, ')
        scripts.append('\n') 
        scripts.append(script_select) 
        scripts.append('\n')  
        scripts.append('FROM ' + schema_redshift_destinate + '.' + raw_table['table_alias']) 
        scripts.append('\n')
        scripts.append(') t WHERE t.orden = 1;') 
        scripts.append('\n') 
        scripts.append('TRUNCATE TABLE ' + schema_redshift_destinate + '.' + raw_table['table_alias'] + ';')              
        scripts.append('\n') 
        scripts.append('INSERT INTO ' + schema_redshift_destinate + '.' + raw_table['table_alias'])
        scripts.append('\n') 
        scripts.append('SELECT ' + script_select)
        scripts.append('\n') 
        scripts.append('FROM #' + raw_table['table_alias'] + ';') 
        scripts.append('\n')
        scripts.append('END;')
        scripts.append('\n')
        scripts.append('$$;')
        scripts.append('\n')
         
        return scripts 
    
    def get_script_drop_table_raw_redshift(self, schema_redshift, raw_table):
        scripts = []
        scripts.append('DROP TABLE IF EXISTS ' + schema_redshift + '.' + raw_table['table_name'] ) 
        return scripts
    
    def get_script_drop_function_raw_redshift(self, schema_redshift_source, raw_table):
        scripts = []
        scripts.append('DROP PROCEDURE  ' + schema_redshift_source + '.usp_load_' + raw_table['table_alias'] + '()') 
        return scripts
    
    def get_script_drop_function_initial_raw_redshift(self, schema_redshift_source, raw_table):
        scripts = []
        scripts.append('DROP PROCEDURE  ' + schema_redshift_source + '.usp_load_initial_' + raw_table['table_alias'] + '()') 
        return scripts    
    
    
    
        