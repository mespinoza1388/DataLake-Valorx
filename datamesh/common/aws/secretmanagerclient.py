from datamesh.common.aws.base import base
class secretmanagerclient(base):
    def __init__(self, region_name, config = None):
         super().__init__(config)
         self.__client_sm = self.session.client(service_name = 'secretsmanager', region_name = region_name)
 
    def get_secret_value(self, secret_id):
        try:      
            response = self.__client_sm.get_secret_value(SecretId=secret_id)
            return True, eval(response['SecretString'])
        except Exception as ex:
            return False, str(ex)