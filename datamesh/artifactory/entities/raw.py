import datamesh.artifactory.config.constants as constants
class raw():     
    def __init__(self, config_datalake = None):  
        index_col_tables = constants.entities_raw_index_col_tables
        index_col_columns = constants.entities_raw_index_col_columns
        file_tables = constants.entities_raw_tables
        file_columns = constants.entities_raw_columns
        separator = constants.entities_separador
        header = constants.entities_header  
        if not config_datalake is None:  
            if constants.entities_type_source == 'aws_s3':     
                bucket_name = config_datalake['bucket_artifactory']
                path_key = config_datalake['path_artifactory_raw']  
                status, data = self.__get_data_s3(file_tables, separator, index_col_tables, header, bucket_name, path_key) 
                if status:
                    self.__data_tables = data 
                else:
                    raise Exception(data)
                status, data = self.__get_data_s3(file_columns, separator, index_col_columns, header, bucket_name, path_key) 
                
                if status:
                    self.__data_columns = data 
                else:
                    raise Exception(data)
        else:
            raise NotImplementedError('Not implement function')

    def __get_data_s3(self, file_name, separator, index_col, header, bucket_name, path_key): 
        from datamesh.common.aws.s3client import s3client
        import pandas as pd
        s3_client = s3client()
        status_code, file_tables = s3_client.get_object(bucket_name, path_key + file_name)   
        if status_code:
            try:
                df = pd.read_csv(file_tables, sep=separator, index_col = index_col, header = header)
                df = df.fillna('')
                df = df.sort_index()
                return True, df
            except Exception as ex:
                return False, str(ex)
        else:
            return False, file_tables
          
    def tables(self, table_type=None, table_name=None, schema_group=None, status='a'): 
        data = [] 
        if not self.__data_tables is None:
            df = self.__data_tables
            for i in df.index: 
                if (str(df['table_type'][i]).upper() == table_type.upper() if table_type is not None else str(df['table_type'][i]).upper()) \
                    and (str(i[1]).upper() == table_name.upper() if table_name is not None else str(i[1]).upper()) \
                    and (str(i[0]).upper() == schema_group.upper() if schema_group is not None else str(i[0]).upper()) \
                    and (str(df['status'][i]).upper() == status.upper() if status is not None else str(df['status'][i]).upper()):
                    data.append({
                        'schema_group' : i[0],
                        'table_name' : i[1], 
                        'table_type' : df['table_type'][i],
                        'table_alias' : df['table_alias'][i],
                        'table_priority' : df['table_priority'][i],
                        'process_id' : df['process_id'][i],
                        'column_pk' : df['column_pk'][i],
                        'column_lastmodifydate' : df['column_lastmodifydate'][i],
                        'has_join' : df['has_join'][i],  
                        'exp_join' : df['exp_join'][i],  
                        'has_filter' : df['has_filter'][i],
                        'exp_filter' : df['exp_filter'][i],  
                        'type_filter_full' : df['type_filter_full'][i],
                        'exp_filter_full' : df['exp_filter_full'][i], 
                        'type_filter_incremental' : df['type_filter_incremental'][i], 
                        'exp_filter_incremental' : df['exp_filter_incremental'][i], 
                        'delay_incremental_ini' : df['delay_incremental_ini'][i], 
                        'delay_incremental_end' : df['delay_incremental_end'][i], 
                        'type_limit_period' : df['type_limit_period'][i],
                        'query_limit_period' : df['query_limit_period'][i],
                        'status' : df['status'][i] 
                    }) 
        else:
            raise Exception('error cargando raw_tables')
        return data 
 
    def columns(self, table_name=None, schema_group=None, status='a'): 
        data = []   
        if not self.__data_columns is None:
            df = self.__data_columns
            for i in df.index:  
                if (str(i[1]).upper() == table_name.upper() if table_name is not None else str(i[1]).upper()) \
                    and (str(i[0]).upper() == schema_group.upper() if schema_group is not None else str(i[0]).upper()) \
                    and (str(df['status'][i]).upper() == status.upper() if status is not None else str(df['status'][i]).upper()):
                    data.append({
                        'schema_group' : i[0],
                        'table_name' : i[1],
                        'column_id' : i[2],
                        'column_name' : df['column_name'][i], 
                        'column_type' : df['column_type'][i],
                        'is_column_calculated' : df['is_column_calculated'][i],
                        'exp_column_calculated' : df['exp_column_calculated'][i],
                        'status' : df['status'][i]  
                    })  
        else:
            raise Exception('error cargando raw_columns')
        return data  
 