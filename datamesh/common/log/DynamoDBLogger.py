import logging
import datamesh.common.util.date as date

class DynamoDBLogger(logging.Logger):
    def __init__(self, name, boto3_current, table_name, parameters): 
        super().__init__(name) 
        self.__boto3_current = boto3_current
        self.__table_name = table_name 
        self.__parameters = parameters
          
    def __log_to_dynamo(self, message, level):
        try:
            self.__parameters['create_date'] = date.get_now_format('%Y-%m-%d %H:%M:%S.%f')
            self.__parameters['log_level'] = level
            self.__parameters['log_message'] = message
            item_input = self.__parameters   
            db_resource = self.__boto3_current.resource('dynamodb') 
            db_table = db_resource.Table(self.__table_name)
            db_table.put_item(Item=item_input)
        except Exception as ex:
            print(str(ex))

    def info(self, message, *args, **kwargs): 
        self.__log_to_dynamo(message, 'INFO')

    def warning(self, message, *args, **kwargs):
        self.__log_to_dynamo(message, 'WARNING')

    def error(self, message, *args, **kwargs):
        self.__log_to_dynamo(message, 'ERROR')

    def close(self):
        pass