#from datamesh.artifactory.config.configbase import configbase
from datamesh.common.aws.dynamodbresource import dynamodbresource
import datamesh.artifactory.config.constants as constants

#class configdb(configbase): 
class configdb():     
    def __init__(self, environment, schemas, config = None):   
        self.__prefix = constants.dynamodb_tables_config_prefix
        self.__dynamodb_resource = dynamodbresource(config)
        self.__environment = environment  
        self.__config = {}   
        self.__config['schema'] = {}     
        self.__config['datasource_id'] = ''
        self.__configs_datalake_main()  
        if 'schema_id' in schemas:
            self.__schema_id = schemas['schema_id']
            self.__datasource_id = "-".join(schemas['schema_id'].split('-')[0:2])
            self.__configs_datalake_layer() 
            self.__configs_schema_layer()
        if 'schema_analytics_id' in schemas:
            self.__schema_analytics_id = schemas['schema_analytics_id']
            self.__configs_datalake_analytics()  
    def __configs_datalake_main(self):  
        self.__config['datalake'] = {}
        db_table = self.__prefix + '_' + self.__environment + '_datalake_config'
        status, response = self.__dynamodb_resource.get_item_by_key(db_table,'bucket_type', 'raw')
        if status:
            self.__config['datalake']['bucket_raw'] = response['bucket_name']
        else:
            raise Exception('No se pudo leer la tabla: ' + db_table + ' message: ' + response)
        status, response = self.__dynamodb_resource.get_item_by_key(db_table,'bucket_type', 'stage')
        if status:
            self.__config['datalake']['bucket_stage'] = response['bucket_name']
        else:
            raise Exception('No se pudo leer la tabla: ' + db_table + ' message: ' + response)
        status, response = self.__dynamodb_resource.get_item_by_key(db_table,'bucket_type', 'analytics')
        if status:
            self.__config['datalake']['bucket_analytics'] = response['bucket_name'] 
        else:
            raise Exception('No se pudo leer la tabla: ' + db_table + ' message: ' + response)
        status, response = self.__dynamodb_resource.get_item_by_key(db_table,'bucket_type', 'artifactory')
        if status:
            self.__config['datalake']['bucket_artifactory'] = response['bucket_name'] 
        else:
            raise Exception('No se pudo leer la tabla: ' + db_table + ' message: ' + response)
        status, response = self.__dynamodb_resource.get_item_by_key(db_table,'bucket_type', 'athena')
        if status:
            self.__config['datalake']['bucket_athena'] = response['bucket_name']  
        else:
            raise Exception('No se pudo leer la tabla: ' + db_table + ' message: ' + response)
    
    def __configs_schema_layer(self): 
        db_table =  self.__prefix + '_' + self.__environment + '_datalake_config_schema'            
        status, response = self.__dynamodb_resource.get_item_by_key(db_table, 'schema_id', self.__schema_id)
        if status:  
            self.__config['schema']['team'] = response['team']
            self.__config['schema']['schema'] = response['schema']
            if 'datasource' in response:
                self.__config['schema']['datasource'] = response['datasource']
                self.__config['schema']['datasource_id'] = response['team'] + '-' + response['datasource'] 
            if 'schema_group' in response:
                self.__config['schema']['schema_group'] = response['schema_group']
            if 'country' in response:
                self.__config['schema']['country'] = response['country']
            if 'config' in response:
                self.__config['schema']['config'] = response['config'] 
        else:
            raise Exception('No se pudo leer la tabla: ' + db_table + ' message: ' + response)
                
    def __configs_datalake_layer(self):  
        db_table = self.__prefix + '_' + self.__environment + '_datalake_config_datasource' 
        status, response = self.__dynamodb_resource.get_item_by_key(db_table,'datasource_id', self.__datasource_id)
        if status:  
            if 'config' in response:      
                self.__config['datalake']['config'] = response['config'] 
            if 'path_artifactory_raw' in response:
                self.__config['datalake']['path_artifactory_raw'] = response['path_artifactory_raw'] 
            if 'path_artifactory_stage' in response:
                self.__config['datalake']['path_artifactory_stage'] = response['path_artifactory_stage'] 
        else:
            raise Exception('No se pudo leer la tabla: ' + db_table + ' message: ' + response)
    
    def __configs_datalake_analytics(self):   
        db_table = self.__prefix + '_' + self.__environment + '_datalake_config_schema_analytics'
        status, response = self.__dynamodb_resource.get_item_by_key(db_table,'schema_id', self.__schema_analytics_id)
        if status:  
            if 'path_artifactory_analytics' in response:
                self.__config['datalake']['path_artifactory_analytics'] = response['path_artifactory_analytics']  
            if 'schema_analytics' in response:
                self.__config['schema']['schema_analytics'] = response['schema_analytics']  
            if 'team' in response:
                self.__config['schema']['team'] = response['team'] 
        else:
            raise Exception('No se pudo leer la tabla: ' + db_table + ' message: ' + response)
    
    def config_datalake(self):
        return self.__config['datalake']
    
    def config_schema(self):
        return self.__config['schema']
     