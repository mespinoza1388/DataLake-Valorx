from datetime import date, datetime, timedelta
from dateutil.relativedelta import relativedelta
import calendar  

def get_date(year, month, day):
    return date(year, month, day)

def get_date_format(value, format):
    return value.strftime(format) 

def get_today():
    return date.today()

def get_now():
    return datetime.now()

def get_date_from_datemagic(datemagic): 
    try:
        if datemagic is None:
            value_date = datetime(1900,1,1,0,0,0)
        else:
            float_date = float(datemagic)
            int_date = int(float_date)
            if int_date > 3652059:
                value_date = datetime(9999,12,31,0,0,0)
            else:
                if int_date < 693595:
                    value_date = datetime(1900,1,1,0,0,0) 
                else:
                    value_date = datetime(1900, 1, 1, 0, 0, 0) + timedelta(days = int_date - 693596) 
    except:
        value_date = datetime(1900,1,1,0,0,0)
    return value_date

def get_datemagic_from_date(value_date): 
    try:
        if value_date is None:
            value_datemagic = 693596
        else:
            date_ini = date(1900,1,1)
            date_end = value_date 
            diference = date_end - date_ini
            value_datemagic = 693596 + diference.days
    except Exception as ex:
        value_datemagic = 693596
        print(str(ex))
    return value_datemagic

def add_seconds(value, sencods_add, format = None):
    if format is None:
        value_obj = value
    else:
        value_obj = datetime.strptime(value, format) 
    value_obj = value_obj + timedelta(seconds=sencods_add)
    return value_obj

def add_days(value, days_add, format = None):
    if format is None:
        value_obj = value
    else:
        value_obj = date.strptime(value, format) 
    value_obj = value_obj + timedelta(days=days_add)
    return value_obj

def add_months(value, months_add, format = None):
    if format is None:
        value_obj = value
    else:
        value_obj = date.strptime(value, format) 
    value_obj = value_obj + relativedelta(months=months_add)
    return value_obj

def add_years(value, years_add, format = None):
    if format is None:
        value_obj = value
    else:
        value_obj = date.strptime(value, format) 
    value_obj = value_obj + relativedelta(years=years_add)
    return value_obj

def add_processperiod(value, periods_add, type = 'S'):
    value_obj = value + relativedelta(months=periods_add)
    year = value_obj.year
    month = value_obj.month 
    if type == 'S':
        fecha = date(year, month, 1)
    elif type == 'E':
        ultimo_dia = calendar.monthrange(year, month)[1]
        fecha = date(year, month, ultimo_dia)
    return fecha 

def get_today_format(format):    
    return date.today().strftime(format) 

def get_now_format(format):
    return datetime.now().strftime(format)
 
def next_period(period, type_period): 
    year = int(period / 100)
    month = period % 100 
    if type_period.upper() == 'M':
        if month > 11:
            month = 1
            year = year + 1
        else:
            month = month + 1
        period = year * 100 + month 
    if type_period.upper() == 'Y':
        period = (year + 1) * 100 + month
    if type_period.upper() == 'S':
        if month > 6:
            if month == 12:
                month = 6
            else:
                month = month % 6
            year = year + 1
        else:
            month = month + 6
        period = year * 100 + month 
    if type_period.upper() == 'T':
        if month > 9:
            if month == 12:
                month = 3
            else:
                month = month % 3
            year = year + 1 
        else:
            month = month + 3 
        period = year * 100 + month 
    return period

def last_day_period(period):
    year = int(period / 100)
    month = period % 100
    return calendar.monthrange(year, month)[1]

def get_periods(data_resumen, type_period):  
    periods = [] 
    period_ini = data_resumen['datetime_start'].year * 100 + data_resumen['datetime_start'].month
    period_fin = data_resumen['datetime_end'].year * 100 + data_resumen['datetime_end'].month   
    period_current = period_ini 
    while period_current <= period_fin:  
        period_start = period_current 
        period_end = next_period(period_current, type_period) 
        periods.append({
            'datetime_start':datetime(int(period_start / 100), period_start % 100,1,0,0,0),
            'datetime_end':datetime(int(period_end / 100), period_end % 100, last_day_period(period_end),23,59,59),
            'period_start' : str(period_start),
            'period_end' : str(period_end)
        })  
        period_current = next_period(period_end, 'm') 
    return periods

def get_epochs(periods, cycles): 
    epochs = [] 
    len_periods = len(periods)
    if len_periods < cycles:
        epochs.append(periods)
    else:
        for i in range(int(len_periods / cycles) + 1): 
            current_epoch = periods[i * cycles: (i + 1) * cycles]
            if len(current_epoch) > 0:
                epochs.append(current_epoch)
    return epochs

