from datamesh.services.extractor.sqlserver_extractor_service import sqlserver_extractor_service
from datamesh.common.datasource.sparksql import sparksql 
from datamesh.common.log.FileLogger import FileLogger   
from datamesh.common.aws.s3client import s3client
import datamesh.common.util.date as date 
import boto3
import sys 
import os


#########################  ARGUMENTO  ##################################
import argparse 
parser = argparse.ArgumentParser()
parser.add_argument("-s", "--schema", help="Nombre del Schema")
parser.add_argument("-tp", "--type_process", help="f => Extractor Full, i => Extractor incremental")
parser.add_argument("-tt", "--type_table", help="m => Maestras, t => Transaccional, all => Todas")
parser.add_argument("-d", "--path_date", help="Periodo a procesar: yyyy/mm/dd en caso contrario obtiene la fecha actual")
#########################################################################
  
def getResolvedOptionsOptional(args_mandatory, args_optional_default): 
    from awsglue.utils import getResolvedOptions
    given_optional_fields_key = list(set([i[2:] for i in sys.argv]).intersection([i for i in args_optional_default])) 
    args = getResolvedOptions(sys.argv, args_mandatory + given_optional_fields_key) 
    args_optional_default.update(args)
    return args_optional_default
 
def execute_query_to_dataframe(query): 
    try:
        log.info(query)
        sparkSQL = sparksql(V_DRIVER_BD_SOURCE, V_DRIVER_PROCESS, con_params)
        status, df = sparkSQL.execute_query_to_dataframe(query) 
        return status, df
    except Exception as ex:        
        return False, str(ex)
  
def get_period_config_limit(entity): 
    try:  
        if entity['type_limit_period'] != '':
            query = entity['query_limit_period'] 
            log.info(query)
            sparkSQL = sparksql(V_DRIVER_BD_SOURCE, V_DRIVER_PROCESS, con_params)
            status, df = sparkSQL.execute_query_to_dataframe(query) 
            if status:
                periods = []
                log.info('Se ejecutó el query para obtener los periodos límites')
                if V_DRIVER_PROCESS == 'PANDAS':
                    for i in df.index: 
                        if entity['type_limit_period'].upper() == 'AJE_DATE': 
                            periods.append((df['min_date'][i], df['max_date'][i]))
                        if entity['type_limit_period'].upper() == 'AJE_PERIOD':
                            periods.append((df['min_period'][i], df['max_period'][i])) 
                else:
                    for data_row in df.collect():
                        if entity['type_limit_period'].upper() == 'AJE_DATE':
                            ele = data_row.asDict()
                            periods.append((ele['min_date'], ele['max_date']))
                        if entity['type_limit_period'].upper() == 'AJE_PERIOD':
                            ele = data_row.asDict()
                            periods.append((ele['min_period'], ele['max_period']))
                periods.sort()
                log.info(str(periods))    
                if len(periods) == 0:
                    log.warning('No se encontraron periodos')
                    return False, []
                if len(periods) == 1:
                    data_period = periods[0]
                    if data_period[0] is None or data_period[1] is None:
                        log.warning('Los periodos no se encuentran seteados')
                        return False, []  
                return True, periods            
            else:
                log.error(df)
                return False, []
        else:
            return False, []
    except Exception as ex: 
        log.error(ex)
        return False, []
   
def write_dataframe_to_s3(df_input, entity, date_path, configs):
    try: 
        if V_DRIVER_PROCESS == 'PANDAS':
            obj_config = configs['pandas']
            file_name = 'file_' + entity['table_name'] + '_' + date.get_date_format(date.get_now(),"%Y%m%d_%H%M%S") + '.csv'
            #path_s3 = path_s3 + '/' + file_name
            obj_config['bucket_s3'] = datalake['bucket_raw']
            obj_config['path_s3_file'] = service.get_path_raw_s3() + '/' +  entity['table_name'] + '/' + date_path   + '/' + file_name
            obj_config['path_local_file'] = obj_config['localpath'] + file_name
        else:
            obj_config = configs['spark']
            obj_config['path_s3'] = service.get_path_full_raw_s3() + '/' +  entity['table_name'] + '/' + date_path  
        log.info(str(obj_config))
        status, response = sparksql.save_file(V_DRIVER_PROCESS, df_input, obj_config) 
        log.info(response) 
        return status, response
    except Exception as ex: 
        return False, str(ex)

def delete_folder_s3(entity): 
    path_s3 = service.get_path_raw_s3() + '/' +  entity['table_name']
    status, response = s3_client.delete_folder_and_objects_inside(datalake['bucket_raw'], path_s3)
    if status:
        log.info('se eliminó la carpeta en S3: ' + path_s3)
    else:
        log.error(str(response))

def save_last_modifydate(df_input, entity):
    log.info('Se inicial el proceso para guardar last_modifydate')
    if entity['type_filter_incremental'].upper() == 'AJE_LASTMODIFY':
        if entity['column_lastmodifydate'] != '':  
            #status, max_column = sparksql.get_column_max_to_dataframe(V_DRIVER_PROCESS, df_input, entity['column_lastmodifydate'])
            max_column = df_input
            log.info('Se obtuvo el lastmodifydate: ' + str(max_column))
            if status: 
                status, response = service.put_config_table_audit(entity['table_name'], max_column, V_PROCESS_ID) 
                if not status: 
                    log.error(response)
            else:
                log.warning('No se encontraron registros, no se pudo obtener el lastmodifydate') 
        else:
            log.warning('No se encuentra configurado el campo column_lastmodifydate') 

def union_dataframe(df_A, df_B):
    return sparksql.union_dataframe(V_DRIVER_PROCESS, df_A, df_B)

def get_period_config(entity): 
    if entity['type_filter_incremental'].upper() == 'AJE_LASTMODIFY':
        status, config_table_audit = service.get_config_table_audit(entity['table_name']) 
        if status and config_table_audit['lastmodifydate'] != '': 
            date_ini = date.add_seconds(config_table_audit['lastmodifydate'], 1, '%Y-%m-%d %H:%M:%S')
            date_end = date.get_today() 
            return (date_ini, date_end)
    elif entity['type_filter_incremental'].upper() == 'AJE_PERIOD': 
        period_date_ini = date.add_months(date.get_today(), entity['delay_incremental_ini'])
        period_date_end = date.add_months(date.get_today(), entity['delay_incremental_end'])
        return (period_date_ini.year * 100 + period_date_ini.month, period_date_end.year * 100 + period_date_end.month)
    elif entity['type_filter_incremental'].upper() == 'AJE_DATE': 
        period_date_ini = date.add_days(date.get_today(), entity['delay_incremental_ini'])
        period_date_end = date.add_days(date.get_today(), entity['delay_incremental_end'])
        return (date.get_datemagic_from_date(period_date_ini), date.get_datemagic_from_date(period_date_end))
    elif entity['type_filter_incremental'].upper() == 'AJE_PROCESSPERIOD': 
        period_date_ini = date.add_processperiod(date.get_today(), entity['delay_incremental_ini'], 'S')
        period_date_end = date.add_processperiod(date.get_today(), entity['delay_incremental_end'], 'E')
        return (date.get_datemagic_from_date(period_date_ini), date.get_datemagic_from_date(period_date_end))
    else:
        return None

def process_full(table_type, process_id=None, table_name=None):    
    type_process = 'F'
    log.info('inicia proceso type_process: ' + type_process + ' table_type: ' + table_type)
    entities = service.get_raw_entities(type_process = type_process, table_type = table_type, schema_group = schema['schema_group'], process_id = process_id, table_name = table_name)   
    for entity in entities:  
        log.info(str(entity))
        process_date = date.get_now_format('%Y-%m-%d %H:%M:%S') 
        delete_folder_s3(entity) 
        date_current = date.get_date(2000, 1, 1)
        date_path = date.get_date_format(date_current, '%Y/%m/%d')   
        status, data_periods = get_period_config_limit(entity)   
        if status:
            data_epochs = date.get_epochs(data_periods, 2) 
            data_epochs.sort()
            log.info(str(data_epochs)) 
            for data_epoch in data_epochs: 
                data_periods = data_epoch
                data_period = data_periods[0]
                query = service.get_custom_query(entity, type_process, data_period) 
                status, df_current = execute_query_to_dataframe(query) 
                log.info('se ejecuto el query')
                if status: 
                    data_periods = data_periods[1:] 
                    for data_period in data_periods:
                        query = service.get_custom_query(entity, type_process, data_period)  
                        status, df_input = execute_query_to_dataframe(query)
                        log.info('se ejecuto el query') 
                        if status:  
                            df_current = union_dataframe(df_current, df_input) 
                        else:
                            log.error(df_input)
                    write_dataframe_to_s3(df_current, entity, date_path, datalake['config']) 
                    #save_last_modifydate(df_current, entity)
                    save_last_modifydate(process_date, entity)
                else:
                    log.error(df_current)
                date_current = date.add_days(date_current, 1)
                date_path = date.get_date_format(date_current, '%Y/%m/%d') 
        else:
            query = service.get_custom_query(entity, type_process) 
            status, df_current = execute_query_to_dataframe(query)
            log.info('se ejecuto el query')
            if status: 
                write_dataframe_to_s3(df_current, entity, date_path, datalake['config']) 
                #save_last_modifydate(df_current, entity) 
                save_last_modifydate(process_date, entity) 
            else:
                log.error(df_current) 
 
def process_incremental(table_type, process_id=None, table_name=None): 
    type_process = 'I'
    log.info('inicia proceso type_process: ' + type_process + ' table_type: ' + table_type)
    entities = service.get_raw_entities(type_process = type_process, table_type = table_type, schema_group = schema['schema_group'], process_id = process_id, table_name = table_name)   
    for entity in entities:   
        log.info(str(entity)) 
        process_date = date.get_now_format('%Y-%m-%d %H:%M:%S') 
        date_current = date.get_today()
        date_path = date.get_date_format(date_current, '%Y/%m/%d') 
        date_period = get_period_config(entity)
        query = service.get_custom_query(entity, type_process, date_period)   
        status, df_current = execute_query_to_dataframe(query) 
        if status: 
            write_dataframe_to_s3(df_current, entity, date_path, datalake['config'])
            #save_last_modifydate(df_current, entity) 
            save_last_modifydate(process_date, entity) 
        else:
            log.error(df_current)  


############################################################################################
AWS_ACCESS_KEY_ID="AKIAZWFZEHOTANVNSJFG"
AWS_SECRET_ACCESS_KEY="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"
AWS_REGION = 'us-east-1' 
boto3.setup_default_session(aws_secret_access_key=AWS_SECRET_ACCESS_KEY, aws_access_key_id=AWS_ACCESS_KEY_ID, region_name = AWS_REGION)  
############################################################################################
args = parser.parse_args()   

V_ENVIRONMENT = 'prod' 
V_SCHEMA_ID = args.schema.upper()
V_TYPE_TABLE = args.type_table.upper()
V_TYPE_PROCESS = args.type_process.upper()
V_DRIVER_BD_SOURCE = 'SQLSERVER' #SQLSERVER - POSTGRESQL - REDSHIFT
V_DRIVER_PROCESS = 'PANDAS' #GLUE - SPARK - PANDAS 
V_PROCESS_NAME = 'DataLake Valorx'
V_JOB_NAME = 'Process ETL ' + V_SCHEMA_ID
V_PROCESS_ID = V_SCHEMA_ID + '_' + date.get_now_format('%Y%m%d_%H%M%S')
    
V_PARAMETERS_DATALAKE = {
        'environment': V_ENVIRONMENT,
        'schema_id': V_SCHEMA_ID, 
        'driver_bd_source': V_DRIVER_BD_SOURCE,
        'driver_process': V_DRIVER_PROCESS
    } 

schemas = {}
schemas['schema_id'] = V_SCHEMA_ID
service = sqlserver_extractor_service(V_ENVIRONMENT, schemas)
#log = DynamoDBLogger(__name__, boto3, 'cas_' + V_ENVIRONMENT + '_datalake_log_process', {'process_id': V_PROCESS_ID})
s3_client = s3client()
log = FileLogger(__name__,  V_PROCESS_ID)
try: 
    datalake = service.get_config_datalake()
    schema = service.get_config_schema() 
    con_params = {
                'has_secret_manager' : 'true',
                'secret_manager_name' : schema['config']['secret_manager']['secret_manager_name'],
                'region_name' : schema['config']['secret_manager']['region_name']
            }
    log.info(str(V_PARAMETERS_DATALAKE))
    
    if V_TYPE_TABLE == 'ALL' or V_TYPE_TABLE == 'M':
        log.info('Cargando maestras...')  
        if V_TYPE_PROCESS == 'F':
            log.info('generate_file_master_full...')  
            process_full('M')
        if V_TYPE_PROCESS == 'I':
            log.info('generate_file_master_incremental...')  
            process_incremental('M')

    if V_TYPE_TABLE == 'ALL' or V_TYPE_TABLE == 'T':
        log.info('Cargando maestras...')  
        if V_TYPE_PROCESS == 'F':
            log.info('generate_file_master_full...')  
            process_full('T')
        if V_TYPE_PROCESS == 'I':
            log.info('generate_file_master_incremental...')  
            process_incremental('T')

    log.close() 
except Exception as ex:
    log.error(str(ex))
    log.close()  

 